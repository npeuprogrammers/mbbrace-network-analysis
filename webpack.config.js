var path = require('path');
var webpack = require('webpack');
var WebpackBuildNotifierPlugin = require('webpack-build-notifier');

module.exports = {
    context: path.join(__dirname, 'src'),
    entry: {
        index: './index.js'
    },
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            }
        ]
    },
    resolve: {
        extensions: ['*', '.js']
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].bundle.js'
    },
    // Sourcemap set-up that works with Chrome Devtools and Workspace mapping on Windows
    devtool: false,
    plugins: [
        new webpack.SourceMapDevToolPlugin({
            // https://webpack.js.org/plugins/source-map-dev-tool-plugin/
            // filename: '[name].js.map',
            module: true,
            columns: true,
            moduleFilenameTemplate: info => `file:///${info.absoluteResourcePath.replace(/\\/g, '/')}`, // Chrome DevTools on Windows with 'workspace mapping' likes to have absolute paths with forward slashes - then it can map sourcemapped files to actual files (see https://github.com/webpack/webpack/issues/6400#issuecomment-361848494)
            noSources: true // don't need the actual source because Chrome Devtools will look up the actual original file
        }),
        new WebpackBuildNotifierPlugin({
            title: "website"
        })
    ],
    mode: 'development'
};